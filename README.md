All work and no play makes Jack a dull boy
============================================

The python script `all_work.py` generates a LaTeX `.tex` document with a user input number of 'All work and ...' text paragraphs. The text format is randomly varied throught the document and double letters, capital letters and random characters are added with low probabiltiy.

Usage
=====

1. `python all_work.py` to generate the latex document
2. Build `output.tex` with your systems latex compiler